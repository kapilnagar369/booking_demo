var express = require('express');
var router = express.Router();
var _ = require('lodash');

var fs = require('fs');



    


   
 
var booked_seats = [];


router.post('/allocate/:no_of_seats', function(req, res, next) {
    let rawdata = fs.readFileSync('public/seats.json');
    let seats = JSON.parse(rawdata);

    var a = seats.a;
    var b = seats.b;
    allSeats = _.concat(a,b);
    var i = 0;
    var no_of_seats = req.params.no_of_seats;
    for (var prop in allSeats) {

        if (i != no_of_seats) {
            if (allSeats[prop].status == 'A') {
                if (allSeats[prop].left != null) {
                    booked_seats.push(allSeats[prop].id);
                } else {
                    booked_seats = [];
                    i = 0;
                    booked_seats.push(allSeats[prop].id);
                }
                i++;
            } else {
                i = 0;
                booked_seats = [];
            }
        }
    }

    if (booked_seats.length != no_of_seats) {
        booked_seats = [];
    }
    if(booked_seats.length>0){
    updatedata(a,b,booked_seats,'R','A');
    }
    res.json( booked_seats);
});


function updatedata(a,b,arr,newstatus,oldstatus)
{
for(var n in arr )
{
  var val= arr[n];

  for (var i in a) {
      if (a[i].id == val) {
        if(oldstatus!=a[i].status)
        {
            return false;
        }
     a[i].status = newstatus;
      }
  }

  for (var j in b) {
      if (b[j].id == val) {

        if(oldstatus!=b[j].status)
        {
            return false;
        }
          b[j].status = newstatus;
      }
  }
}

var finalobj= { "a":a, "b":b};
const jsonString = JSON.stringify(finalobj)
fs.writeFile('public/seats.json', jsonString);  
return true;
}

router.post('/sold/:str', function(req, res, next) {
   let rawdata = fs.readFileSync('public/seats.json');
    let seats = JSON.parse(rawdata);

    var a = seats.a;
    var b = seats.b;
   
    str=req.params.str;

         arr=str.split(",");
         if(arr.length>0){
           if(updatedata(a,b,arr,'S','R'))
           {
             res.json({error:false,msg:'Seat status updated'});
           }
           else
           {
             res.json({error:true,msg:'Invalid Argument'});
           }
        }
        else
           {
             res.json({error:true,msg:'Invalid Argument'});
           }
});

router.post('/available/:str', function(req, res, next) {
   let rawdata = fs.readFileSync('public/seats.json');
    let seats = JSON.parse(rawdata);

    var a = seats.a;
    var b = seats.b;
   
    str=req.params.str;

         arr=str.split(",");
         if(arr.length>0){
           if(updatedata(a,b,arr,'A','R'))
           {
             res.json({error:false,msg:'Seat status updated'});
           }
           else
           {
             res.json({error:true,msg:'Invalid Argument'});
           }
        }
        else
           {
             res.json({error:true,msg:'Invalid Argument'});
           }
});
module.exports = router;